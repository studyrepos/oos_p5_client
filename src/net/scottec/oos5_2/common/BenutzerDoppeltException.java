package net.scottec.oos5_2.common;

/**
 * Exception
 * Kennzeichnet Dopplung in Datenhaltung
 */
public class BenutzerDoppeltException extends Exception {
    public BenutzerDoppeltException(String message) {
        super(message);
    }
}

package net.scottec.oos5_2.client;

import net.scottec.oos5_2.common.Benutzer;
import net.scottec.oos5_2.common.BenutzerDoppeltException;
import net.scottec.oos5_2.common.BenutzerVerwaltung;
import net.scottec.oos5_2.common.BenutzerVerwaltungAdmin;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;

public class ClientOrb implements BenutzerVerwaltung {

    private Client client;
    private BenutzerVerwaltung benutzerVerwaltungAdmin;

    public ClientOrb(Client _client, String _filename, boolean _vorhandeneUeberschreiben) {
        this.client = _client;
        this.benutzerVerwaltungAdmin = new BenutzerVerwaltungAdmin(_filename, _vorhandeneUeberschreiben);
    }


     public boolean benutzerOk(Benutzer _benutzer) {
        String address = this.client.getAddress();
        if (address == null)
            return this.benutzerVerwaltungAdmin.benutzerOk(_benutzer);

        try {
            Socket socket = new Socket(address, 4711);
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

            oos.writeInt(1);
            oos.writeUTF(_benutzer.userId);
            oos.writeUTF(Arrays.toString(_benutzer.passWort));
            oos.flush();

            String res = ois.readUTF();
            socket.close();
            if (res.equals("OK"))
                return true;
            else
                return false;


        } catch (IOException exp) {
            exp.printStackTrace();
            return false;
        }
    }

    public void benutzerEintragen(Benutzer _benutzer) throws BenutzerDoppeltException {
        String address = this.client.getAddress();
        try {
            if (address == null)
                this.benutzerVerwaltungAdmin.benutzerEintragen(_benutzer);
            else {
                Socket socket = new Socket(address, 4711);
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

                oos.writeInt(2);
                oos.writeUTF(_benutzer.userId);
                oos.writeUTF(Arrays.toString(_benutzer.passWort));
                oos.flush();

                String res = ois.readUTF();

                socket.close();

                if (res.equals("DOPPELT"))
                    throw new BenutzerDoppeltException("Benutzer bereits vorhanden");
            }
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }

}

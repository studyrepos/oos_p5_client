package net.scottec.oos5_2.client.gui.login;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import net.scottec.oos5_2.client.Client;
import net.scottec.oos5_2.common.Benutzer;

public class LoginController {

    private Client main;
    private boolean neuAnmeldung = false;

    @FXML
    private TextField txtUserId;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private TextField txtAddress;

    @FXML
    private CheckBox cbNeuAnmeldung;

    @FXML
    private CheckBox cbRemote;

    @FXML
    private Button btSubmit;

    @FXML
    private Label lbError;


    public void setMain(Client _main) {
        this.main = _main;
    }

    @FXML
    public void onSubmitClick(Event event) {
        this.lbError.setVisible(false);
        if (this.neuAnmeldung) {
            ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
            this.main.neuAnmeldung(cbRemote.isSelected(), txtAddress.getText());
        }
        else {
            Benutzer benutzer = new Benutzer(this.txtUserId.getText(), this.txtPassword.getText().toCharArray());
//            ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
            this.main.benutzerLogin(benutzer, cbRemote.isSelected(), txtAddress.getText());
        }
    }

    @FXML
    public void onNeuAnmeldungToggle(Event event) {
        this.neuAnmeldung = cbNeuAnmeldung.isSelected();
    }

    @FXML
    public void onRemoteToggle(Event event) {
        this.txtAddress.setDisable(!cbRemote.isSelected());
    }

    public void showError(String... _errorMessage) {
        if (_errorMessage.length != 0)
            this.lbError.setText(_errorMessage[0]);
        this.lbError.setVisible(true);
    }
}

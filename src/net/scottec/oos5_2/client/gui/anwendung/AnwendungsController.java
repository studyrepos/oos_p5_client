package net.scottec.oos5_2.client.gui.anwendung;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import net.scottec.oos5_2.client.Client;

public class AnwendungsController {

    private Client main;

    @FXML
    Label lbMessage;

    @FXML
    Button btCancel;

    public void setMain(Client _main) {
        this.main = _main;
    }

    @FXML
    public void onCancelClick(Event event) {
        System.out.println("[AnwendungsController] Event : Abbrechen");
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
    }
}

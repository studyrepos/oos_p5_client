package net.scottec.oos5_2.client.gui.init;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import net.scottec.oos5_2.client.Client;

public class InitController {

    private Client main;

    @FXML
    private Button btYes;

    @FXML
    private Button btNo;

    public void setMain(Client _main) {
        this.main = _main;
    }

    @FXML
    public void onYesClick(Event event) {
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
        this.main.init(true);
}

    @FXML
    public void onNoClick(Event event) {
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
        this.main.init(false);
    }
}

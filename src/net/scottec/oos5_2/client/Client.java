package net.scottec.oos5_2.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.scottec.oos5_2.client.gui.anwendung.AnwendungsController;
import net.scottec.oos5_2.client.gui.init.InitController;
import net.scottec.oos5_2.client.gui.login.LoginController;
import net.scottec.oos5_2.client.gui.register.AnmeldungsController;
import net.scottec.oos5_2.common.Benutzer;
import net.scottec.oos5_2.common.BenutzerDoppeltException;
import net.scottec.oos5_2.common.BenutzerVerwaltung;


import java.io.IOException;

public class Client extends Application {

    // instance of BenutzerVerwaltungAdmin
    private BenutzerVerwaltung benutzerVerwaltung;

    private boolean remote = false;
    private String address;

    private Stage loginStage, registerStage, anwendungStage;
    private LoginController loginController;
    private AnmeldungsController anmeldungsController;
    private AnwendungsController anwendungsController;

    @Override
    public void start(Stage primaryStage) {

        try {
            Parent init;
            // create loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/init/init.fxml"));
            init = loader.load();
            ((InitController) loader.getController()).setMain(this);
            primaryStage.setTitle("Benutzerverwaltung");
            primaryStage.setScene(new Scene(init));
            primaryStage.show();
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    public void init(boolean _newInit) {
        if (_newInit)
            this.benutzerVerwaltung = new ClientOrb(this, "user.s", true);
        else
            this.benutzerVerwaltung = new ClientOrb(this, "user.s", false);

        startLogin();
    }

    /*
     * Methoden zum Starten der einzelnen Scences
     */

    private void startLogin() {
        try {
            this.loginStage = new Stage();
            Parent login;
            // create loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/login/login.fxml"));
            login = loader.load();
            // set main reference to controller
            this.loginController = loader.getController();
            this.loginController.setMain(this);
            this.loginStage.setTitle("Benutzerverwaltung");
            this.loginStage.setScene(new Scene(login));
            this.loginStage.show();
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    private void startRegister() {
        try {
            this.registerStage = new Stage();
            Parent register;
            // create loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/register/anmeldung.fxml"));
            register = loader.load();
            // set main reference to controller
            this.anmeldungsController = loader.getController();
            this.anmeldungsController.setMain(this);
            this.registerStage.setTitle("Benutzerverwaltung");
            this.registerStage.setScene(new Scene(register));
            this.registerStage.show();
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    private void startAnwendung() {
        try {
            this.anwendungStage = new Stage();
            Parent anwendung;
            // create loader
            FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/anwendung/anwendung.fxml"));
            anwendung = loader.load();
            // set main reference to controller
            this.anwendungsController = loader.getController();
            this.anwendungsController.setMain(this);
            anwendungStage.setTitle("Benutzerverwaltung");
            anwendungStage.setScene(new Scene(anwendung));
            anwendungStage.show();
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    public String getAddress() {
        if (this.remote)
            return this.address;
        return null;
    }


    /*
     * Methoden gemäß Aufgabenstellung
     */

    public void neuAnmeldung(boolean _remote, String _address) {
        this.remote = _remote;
        this.address = _address;

        startRegister();
    }

    public void neuerBenutzer(Benutzer _benutzer) {
        try {
            this.benutzerVerwaltung.benutzerEintragen(_benutzer);
            this.registerStage.close();
            startLogin();
        }
        catch (BenutzerDoppeltException exp) {
            this.anmeldungsController.showError("Benutzer bereits vorhanden");
        }
    }

    public void benutzerLogin(Benutzer _benutzer, boolean _remote, String _address) {
        this.remote = _remote;
        this.address = _address;
        if (this.benutzerVerwaltung.benutzerOk(_benutzer)) {
            System.out.println("Login success");
            this.loginStage.close();
            startAnwendung();
        } else {
            System.out.println("Login failed");
            this.loginController.showError();
        }
    }

    public static void main(String... args) {
        // start client
        new Client();
        launch();
    }

}
